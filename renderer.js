const { ipcRenderer } = require('electron');

document.getElementById('hello-button').addEventListener('click', event => {
	ipcRenderer.send('message:hello', 'Hello from window');
});

ipcRenderer.on('message:response', (event, args) => {
	console.log(args);
});
