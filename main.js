const { app, BrowserWindow, ipcMain } = require('electron');

function createWindow () {
  const win = new BrowserWindow({
    width: 800,
    height: 600,
    webPreferences: {
      nodeIntegration: true,
      contextIsolation: false
    }
  })

	win.loadFile('index.html');
}

// app.on('ready', () => {

// });

ipcMain.on('message:hello', (event, args) => {
  console.log(args);
  event.sender.send('message:response', 'Hello from the other side');
});

app.whenReady().then(() => {
	createWindow();
});

app.on('window-all-closed', () => {
	if (process.platform !== 'darwin') {
		app.quit();
	}
});
